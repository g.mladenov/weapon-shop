import GameUnit from "./src/game-unit.js";
import gameUnitType from "./common/game-unit-type.js";
import Shop from "./src/shop.js";

let unitOne = new GameUnit(gameUnitType.Wizard, 50, 110, 10, 130, 5, 45);
let unitTwo = new GameUnit(gameUnitType.Wizard, 50, 110, 10, 130, 5, 10);
let items = new Shop();

// See game unit stats
// unitOne.getUnitStats();

// Buy An Item - won't be allowed because the unit hasn't visited the shop yet
// unitOne.buyItem(items.currentItems[0]);

// Buy an item - won't be allowed because the game units of the character and the item doesn't match
// unitOne.getUnitStats();
// unitOne.sendUnitInShop();
// unitOne.buyItem(items.currentItems[2]);

// Buy an item - won't be allowed because the unit hasn't got enough money
// unitTwo.getUnitStats();
// unitTwo.sendUnitInShop();
// unitTwo.buyItem(items.currentItems[0]);

// Buy an item - will be allowed
// unitOne.getUnitStats();
// unitOne.sendUnitInShop();
// unitOne.buyItem(items.currentItems[0]);
// unitOne.getUnitStats();

// Stats are being updated after buying an item
