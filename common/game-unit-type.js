const gameUnitType = {
  Wizard: "Wizard",
  Warrior: "Warrior",
  Rogue: "Rogue",
};

export default gameUnitType;
