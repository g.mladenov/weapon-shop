class Shop {
  currentItems = [
    {
      name: "Crystal Scepter",
      recommendedGameUnit: "Wizard",
      price: 12,
      health: 3,
      strength: 4,
      abilityPower: 10,
      mana: 30,
      agility: 3,
    },

    {
      name: "Warmog's Armor",
      recommendedGameUnit: "Warrior",
      price: 8,
      health: 25,
      mana: 10,
      strength: 15,
      abilityPower: 3,
      agility: 10,
    },

    {
      name: "Divine Sword",
      recommendedGameUnit: "Rogue",
      price: 7,
      health: 5,
      mana: 10,
      strength: 15,
      abilityPower: 5,
      agility: 20,
    },
  ];
}

export default Shop;
