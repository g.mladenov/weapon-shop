class GameUnit {
  currentItems;
  isInShop;

  constructor(
    gameUnitType,
    health,
    mana,
    strength,
    abilityPower,
    agility,
    money
  ) {
    this.gameUnitType = gameUnitType;
    this.health = health;
    this.mana = mana;
    this.strength = strength;
    this.abilityPower = abilityPower;
    this.agility = agility;
    this.money = money;
    this.currentItems = [];
    this.isInShop = false;
  }

  getUnitStats() {
    console.log(
      `Unit Type: ${this.gameUnitType},
      Health: ${this.health},
      Mana: ${this.mana},
      Strength: ${this.strength},
      Ability Power: ${this.abilityPower},
      Agility: ${this.agility},
      Money: ${this.money},
      Current Items: ${this.currentItems},
      Currently in Shop: ${this.isInShop}`
    );
  }

  buyItem(item) {
    if (this.money < item.price) {
      return console.log("Not enough money for this item!");
    }

    if (this.isInShop === false) {
      return console.log("You need to be in the shop in order to buy an item!");
    }

    if (this.gameUnitType !== item.recommendedGameUnit) {
      return console.log("This item is not available for your unit type!");
    }

    this.currentItems.push(item.name);
    this.money = this.money - item.price;
    this.health = this.health + item.health;
    this.mana = this.mana + item.mana;
    this.strength = this.strength + item.strength;
    this.abilityPower = this.abilityPower + item.abilityPower;
    this.agility = this.agility + item.agility;
  }

  sendUnitInShop() {
    this.isInShop = !this.isInShop;
  }
}

export default GameUnit;
